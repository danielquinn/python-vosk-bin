FROM archlinux

RUN pacman --sync --refresh --noconfirm base-devel

COPY * /opt/aur/python-vosk-bin/

RUN useradd --groups wheel user \
  && echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/wheel \
  && chown -R user:user /opt/aur/python-vosk-bin

WORKDIR /opt/aur/python-vosk-bin

RUN su user -c "makepkg --printsrcinfo > .SRCINFO" \
  && su user -c "makepkg -si --noconfirm"
